dicionario = {
    1 : 1,
    2 : 2,
    3 : 3,
    4 : 4
}
print(dicionario) # saida: {1,2,3,4}

# Alterando valores
dicionario[1] = 0
dicionario[2] = 1
dicionario[3] = 2
dicionario[4] = 3
print(dicionario) # saida: {0,1,2,3}

# Adicionando valores
dicionario[5] = 4
dicionario[6] = 5
dicionario[7] = 6
dicionario[8] = 7
print(dicionario) # saida: {0,1,2,3,4,5,6,7}